import React, { Component } from 'react';
import ReactDOM from 'react-dom';
let numeral = require('numeral');
let backdropIMG;

class Card extends Component {

  render() {
    let data = this.props.data
      // if movie ID found, then...



      let production = data.production,
          productionCountries = data.production_countries,
          genres = data.genre,
          totalRevenue = data.revenue,
          productionList = nestedDataToString(production),
          productionCountriesList = nestedDataToString(productionCountries),
          noData = '-',
          genresList = nestedDataToString(genres);

      // conditional statements for no data
       if (data.vote === 'undefined' || data.vote === 0) {
          data.vote = noData
        } else {
          data.vote = data.vote + ' / 10'
        };

      if (totalRevenue === 'undefined' || totalRevenue === 0) {
           totalRevenue = noData
         } else {
           totalRevenue = numeral(data.revenue).format('($0,0)');
         };

      return (
        <div className="col-xs-12 cardcont nopadding">

          <div className="meta-data-container col-xs-12 col-md-12">
            <h1>{data.original_title}</h1>

            <span className="tagline">{data.tagline}</span>
            <p>{data.overview}</p>
            <div className="additional-details">
              <span className="genre-list">{genresList}</span>
              <span className="production-list">{productionList}</span>
              <div className="row nopadding release-details">
                <div className="col-xs-6"> Original Release: <span className="meta-data">{data.release}</span></div>
                <div className="col-xs-6"> Running Time: <span className="meta-data">{data.runtime} mins</span> </div>
                <div className="col-xs-6"> Box Office: <span className="meta-data">{totalRevenue}</span></div>
                <div className="col-xs-6"> Vote Average: <span className="meta-data">{data.vote}</span></div>
              </div>
            </div>
          </div>
        </div>
      )
    }
}


function nestedDataToString(nestedData) {
  let nestedArray = [],
      resultString;
  nestedArray.forEach(function(item, i){
    nestedArray.push(item.name);
  });
  resultString = nestedArray.join(', '); // array to string
  return resultString;
};
module.exports = Card;
